﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain.Entity
{
    public class TicketFrontData
    {
        public int Id { get; set; }
        public int Notificacao { get; set; }
        public string Tecnico  { get; set; }
    }
}
