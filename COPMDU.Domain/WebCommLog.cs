﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class WebCommLog
    {
        public int Id { get; set; }
        public string Uri { get; set; }
        public string Path { get; set; }
        public string Param { get; set; }
        public string Body { get; set; }
        public int ResponseCode { get; set; }
        public MethodType Method { get; set; }
        public DateTime Date { get; set; }

        public enum MethodType
        {
            Post,
            Get,
            Put,
            Delete
        }
    }
}
