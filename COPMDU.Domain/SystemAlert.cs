﻿using System;
using System.Collections.Generic;
using System.Text;

namespace COPMDU.Domain
{
    public class SystemAlert
    {
        public int Id { get; set; }

        public string Message { get; set; }

        public DateTime Date { get; set; }

        public bool Active { get; set; }

        public bool General { get; set; }
    }
}
