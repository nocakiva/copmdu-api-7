﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Domain
{
    public class User : Base
    {
        public City City { get; set; }
        public int? CityId { get; set; }
        public int Prefix { get; set; }
        public int Phone { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public bool ChangePassword { get; set; }
        [NotMapped]
        public string Authenticated { get; set; }
    }
}
