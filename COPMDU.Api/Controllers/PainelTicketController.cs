﻿using COPMDU.Api.ViewModels;
using COPMDU.Infrastructure.Interface;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace COPMDU.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PainelTicketController : Controller
    {
        private readonly ITicketRepository _ticketRepository;

        public PainelTicketController(ITicketRepository ticketRepository)
        {
            _ticketRepository = ticketRepository;
        }

        [HttpGet("{id}")]
        public IActionResult GetTicketInfo([FromRoute] int id)
        {
            try
            {
                var outage = _ticketRepository.Get(id);

                TicketInformation response;
                string Eligible;


                if (outage.City.EnabledInteration == true || outage.SymptomId == 1)
                    Eligible = "Sim";
                else
                    Eligible = "Não";

                if (outage.NotificationFromAtlas)
                {
                    response = new TicketInformation
                    {
                        Ticket = outage.Id,
                        City = outage.City.Name.ToString(),
                        NotEligible = Eligible,
                        Notification = "-",
                        Technician = "-",
                        Contract = outage.Contract.ToString(),
                        LogTabFront = "-",
                        NotificationFront = outage.Notification.ToString(),
                        TechnicianFront = (outage.TechnicUser == null ? "-" : outage.TechnicUser.Name),
                    };
                }
                else
                {
                    response = new TicketInformation
                    {
                        Ticket = outage.Id,
                        City = (outage.City == null ? "-" : outage.City.Name.ToString()),
                        NotEligible = Eligible,
                        Notification = outage.Notification.ToString(),
                        Technician = (outage.TechnicUser == null ? "-" : outage.TechnicUser.Name),
                        Contract = outage.Contract.ToString(),
                        LogTabFront = "-",
                        NotificationFront = "-",
                        TechnicianFront = "-"
                    };
                }

                return Ok(response);
            }
            catch (Exception e)
            {
                return new StatusCodeResult(500);
            }
        }
    }
}
