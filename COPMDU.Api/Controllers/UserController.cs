﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Claims;
using System.Text;
using COPMDU.Domain;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;

namespace COPMDU.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : Controller
    {
        private readonly IConfiguration _config;
        private readonly IUserRepository _userRepository;
        private readonly ILogRepository _logRepository;
        private readonly IResolutionRepository _resolutionRepository;
        private readonly Messager _messager;

        public int MinAndroidVersion => int.Parse(_config["CopMdu:MinAndroidVersion"]);
        public bool EnableAndroidCloseOtherTickets => bool.Parse(_config["CopMdu:EnableAndroidCloseOtherTickets"]);

        public UserController(IConfiguration configuration, IUserRepository userRepository, ILogRepository logRepository, IResolutionRepository resolutionRepository)
        {
            _config = configuration;
            _userRepository = userRepository;
            _logRepository = logRepository;
            _resolutionRepository = resolutionRepository;
            _messager =  new Messager(_config);
        }



        [HttpGet]
        public IEnumerable<User> GetAll()
        {
            try
            {
                return _userRepository.GetAll();
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpGet("GetUser/{partialUsername}")]
        public List<string> GetUser(string partialUsername)
        {
            try
            {
                return _userRepository.GetUsername(partialUsername);
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpGet("GetUser")]
        public List<string> GetUser()
        {
            try
            {
                return _userRepository.GetUsername("");
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpGet("{id}")]
        public ActionResult<User> Get(int id)
        {
            try
            {
                var user = _userRepository.Get(id);

                if (user == null)
                {
                    return NotFound();
                }

                return user;
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult<User> Post([FromBody] UserModel user)
        {
            try
            {
                var usr = new User() { Name = user.Name, Username = user.Username, Password = GeneratePassword(), Email = user.Email, CityId = user.CityId, Phone = user.Phone, Prefix = user.Prefix };
                _userRepository.Add(usr);

                return CreatedAtAction("Get", new { id = usr.Id }, usr);
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpPut]
        public ActionResult<User> Put([FromBody] User user)
        {
            try
            {
                _userRepository.Update(user);
                return CreatedAtAction("Get", new { id = user.Id }, user);
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpDelete("{id}")]
        public ActionResult<User> Delete(int id)
        {
            try
            {
                var user = _userRepository.Get(id);

                if (user == null)
                {
                    return NotFound();
                }

                _userRepository.Delete(user);
                return user;
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        //DEPRECATED retirar apos atualizarem o android
        [AllowAnonymous]
        [HttpPost("Authenticate")]
        public IActionResult Authenticate([FromBody]Login login)
        {
            try
            {
                var user = _userRepository.Authenticate(login);
                if (user != null)
                {
                    _logRepository.Add(new Log { UserId = user.Id, Message = $"Usuário '{user.Username}' logado no sistema na versão {login.Version}" });
                    return Ok(new { id = user.Id, name = user.Name, username = login.Username, token = BuildToken(user), authenticated = true, changeEmail = string.IsNullOrEmpty(user.Email), changePassword = user.ChangePassword, resolution = _resolutionRepository.Get(), minVersion =  MinAndroidVersion, enableSearchTicket = EnableAndroidCloseOtherTickets });
                }
                else
                    return Ok(new { id = 0, name = string.Empty, username = login.Username, token = string.Empty, authenticated = false, changeEmail = false, changePassword = false, minVersion = MinAndroidVersion, enableSearchTicket = EnableAndroidCloseOtherTickets });
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpPost("Recover")]
        public bool Recover([FromBody]Login login)
        {
            try
            {
                var user = _userRepository.GetAll().FirstOrDefault(u => u.Username.Equals(login.Username));
                if (user == null)
                    return false;
                var id = user.Id;

                if (bool.Parse(_config["CopMdu:EnableGeneratePassword"]))
                {
                    user.Password = GeneratePassword();
                    _userRepository.Update(user);
                }

                if (bool.Parse(_config["CopMdu:EnableSendEmail"]))
                    SendEmail(user);
                _logRepository.Add(new Log { UserId = id, Message = $"Recuperação de senha do usuário '{user.Username}'" });
                return true;

            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpPost("Change")]
        public IActionResult Change([FromBody]Login login)
        {
            try
            {
                var id = _userRepository.GetAll().First(u => u.Username.Equals(login.Username)).Id;
                var user = _userRepository.Get(id);

                if (user != null)
                {
                    user.Password = login.Password;
                    user.ChangePassword = false;
                    _userRepository.Update(user);
                }

                return Ok(new { username = user?.Username });
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpPost("ChangeEmail")]
        public IActionResult ChangeEmail([FromBody]Login login)
        {
            try
            {
                var id = _userRepository.GetAll().First(u => u.Username.Equals(login.Username)).Id;
                var user = _userRepository.Get(id);

                if (user != null)
                {
                    user.Email = login.Email;
                    _userRepository.Update(user);
                }

                return Ok(new { username = user?.Username });
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        private string BuildToken(User user)
        {
            //TODO: gravar usuário
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                new Claim(JwtRegisteredClaimNames.Jti, user.Id.ToString()),
            };
            var token = new JwtSecurityToken(_config["Jwt:Issuer"],
              _config["Jwt:Issuer"],
              expires: DateTime.Now.AddMinutes(int.Parse(_config["Jwt:ExpirationMinute"])),
              claims: claims,
              signingCredentials: creds);

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private static string GeneratePassword()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[8];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            return new string(stringChars);
        }
        
        private void SendEmail(User user)
        {
            var to = user.Email;
            var body = string.Format(_config["Messages:PasswordRecovery:Message"], user.Username, user.Password);
            var subject = _config["Messages:PasswordRecovery:Subject"];

            _messager.Send(to, subject, body);
        }
    }

    public class UserModel
    {
        public string Name { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public int CityId { get; set; }
        public int Prefix { get; set; }
        public int Phone { get; set; }
    }
}