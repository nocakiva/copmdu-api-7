﻿using COPMDU.Domain.Entity;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.Util;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.Repository
{
    public class FrontMduRepository : IFrontMdu
    {
        private readonly PageSession _client;
        private readonly IConfiguration _config;

        private string MainUrl => _config["Url:FrontMdu:Uri"];
        private string Notification => _config["Url:FrontMdu:Path:TicketNotification"];
        private string Proxy => _config["Url:FrontMdu:UseProxy"];

        public FrontMduRepository(IConfiguration config, CopMduDbContext db)
        {
            _config = config;
            _client = new PageSession(MainUrl, config, db);
            if (!string.IsNullOrEmpty(Proxy))
                _client.SetProxy(Proxy);
        }

        public async Task<TicketFrontData> GetDetail(int ticket)
        {
            var ticketData = await _client.GetObject<TicketFrontData>(string.Format(Notification, ticket));
            if (ticketData != null)
                ticketData.Id = ticket;
            return ticketData;
        }
    }
}
