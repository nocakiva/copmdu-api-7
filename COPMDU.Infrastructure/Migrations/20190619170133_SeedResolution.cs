﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class SeedResolution : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 5403,
                column: "Active",
                value: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 5403,
                column: "Active",
                value: false);
        }
    }
}
