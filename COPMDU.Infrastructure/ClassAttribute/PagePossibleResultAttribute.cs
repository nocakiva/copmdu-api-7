﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.ClassAttribute
{
    /// <summary>
    /// Adiciona possiblidade no enum de verificar se pode ser um tipo de resposta a um post para uma página web
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Field, Inherited = false, AllowMultiple = true)]
    sealed class PagePossibleResultAttribute : Attribute
    {
        /// <summary>
        /// Se a página de resposta tiver essa string constara como válido
        /// </summary>
        public string ContainString { get; set; } = "";

        /// <summary>
        /// Valida a resposta da página com a expressão regular se tiver alguma definida não irá constar o <see cref="ContainString"/>
        /// </summary>
        public Regex MatchRegex { get; set; }

        /// <summary>
        /// Função para validar a resposta da página irá receber o conteúdo da página e retornar se deve utilizar esse retono se definido não irá utilizar o <see cref="ContainString"/> nem o <see cref="MatchRegex"/>
        /// </summary>
        public Func<string, bool> ValidateFunction { get; set; }
    }
}
