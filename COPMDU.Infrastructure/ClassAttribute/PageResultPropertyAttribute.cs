﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using COPMDU.Infrastructure.Util;

namespace COPMDU.Infrastructure.ClassAttribute
{
    //TODO: migrar tudo isso para 

    /// <summary>
    /// Configura um regex que é utilizado pelo <see cref="StringTransformation"/> para tranformar texto em uma propriedade
    /// Caso tenha múltiplos atributos atribuidos ao encontrar um regex no texto irá utilizar ele e ignorar os subsequentes.
    /// </summary>
    [System.AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
    public class PageResultPropertyAttribute : Attribute
    {
        readonly string _regexString;

        public PageResultPropertyAttribute(string regexString) =>
            _regexString = regexString;

        /// <summary>
        /// Regex que irá utilizar deve conter 1 grupo que será o resultado vinculado a propriedade
        /// </summary>
        public string RegexString => 
            _regexString;

        public ConversionType ValueType { get; set; } = ConversionType.String;

        /// <summary>
        /// Se for falso não irá carregar esse atributo
        /// </summary>
        public bool Active { get; set; } = true;

        /// <summary>
        /// Covert o dado do retorno do regex para o objeto
        /// </summary>
        public Func<string, object> Convert =>
            ConversionDictionary[ValueType];

        private readonly Dictionary<ConversionType, Func<string, object>> ConversionDictionary = new Dictionary<ConversionType, Func<string, object>>
            {
                { ConversionType.String, value =>
                    Regex.Replace(value.Trim(), @"<.*br.*>", "")
                },
                { ConversionType.Integer, value =>
                    value == "" ?
                        0 :
                        int.Parse(value.Replace("-", "").Replace(".", ""))
                },
                { ConversionType.Long, value =>
                    value == "" ?
                        0 :
                        long.Parse(value.Replace("-", "").Replace(".", ""))
                },
                { ConversionType.Date, value =>
                    value == "" ?
                        new DateTime(1, 1, 1) :
                        DateTime.ParseExact(value, "dd/MM/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture)
                },
                { ConversionType.Boolean, value =>
                    new []{"sim", "s", "y", "yes", "1"}.Contains(value.ToLower()) &&
                    !string.IsNullOrWhiteSpace(value)
                },
            };

        public enum ConversionType
        {
            String,
            Integer,
            Date,
            Boolean,
            Long,
        }
    }
}
